#include "stdafx.h"
#include "Renderer.h"

Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);

	Class0310();
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_SolidRectShader = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	m_ParticleShader= CompileShaders("./Shaders/Particle.vs", "./Shaders/Particle.fs");
	m_FragmentSandboxShader = CompileShaders("./Shaders/FragmentTest.vs", "./Shaders/FragmentTest.fs");
	m_FragmentSandbox_BlendShader = CompileShaders("./Shaders/FragmentAlphaBlend.vs", "./Shaders/FragmentAlphaBlend.fs");
	m_VertexMovementShader = CompileShaders("./Shaders/VertexMovement.vs", "./Shaders/VertexMovement.fs");
	
	//Create VBOs
	CreateVertexBufferObjects();

	CreateParticleVBO(10000);
	CreateParticleVertexVBO(1000);


	CreateFragmentSandboxVBO(1);
	CreateFragmentSandbox_BlendVBO();


	CreateVertexMovementVBO(100);

	if (m_SolidRectShader > 0 && m_VBORect > 0)
	{
		m_Initialized = true;
	}
}

bool Renderer::IsInitialized()
{
	return m_Initialized;
}

void Renderer::CreateVertexBufferObjects()
{
	float rect[]
		=
	{
		-1.f / m_WindowSizeX, -1.f / m_WindowSizeY, 0.f, -1.f / m_WindowSizeX, 1.f / m_WindowSizeY, 0.f, 1.f / m_WindowSizeX, 1.f / m_WindowSizeY, 0.f, //Triangle1
		-1.f / m_WindowSizeX, -1.f / m_WindowSizeY, 0.f,  1.f / m_WindowSizeX, 1.f / m_WindowSizeY, 0.f, 1.f / m_WindowSizeX, -1.f / m_WindowSizeY, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBORect);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);
}

void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done." << std::endl;

	return ShaderProgram;
}

void Renderer::DrawSolidRect(float x, float y, float z, float size, float r, float g, float b, float a)
{
	float newX, newY;

	GetGLPosition(x, y, &newX, &newY);

	//Program select
	glUseProgram(m_SolidRectShader);

	glUniform4f(glGetUniformLocation(m_SolidRectShader, "u_Trans"), newX, newY, 0, size);
	glUniform4f(glGetUniformLocation(m_SolidRectShader, "u_Color"), r, g, b, a);

	int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
	glEnableVertexAttribArray(attribPosition);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(attribPosition);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void Renderer::GetGLPosition(float x, float y, float *newX, float *newY)
{
	*newX = x * 2.f / m_WindowSizeX;
	*newY = y * 2.f / m_WindowSizeY;
}



void Renderer::CreateParticleVBO(int nParticleCnt)
{
	srand(time(NULL));

	// Particle Count
	float size = 0.01f;
	int particleCnt = nParticleCnt;
	m_ParticleVerticesCnt = particleCnt * 6;

	// Position
	int Position_floatCnt = particleCnt * 6 * 3;
	float* vertices_Position = NULL;
	vertices_Position = new float[Position_floatCnt];

	int Position_idx = 0;

	float centerX, centerY;
	centerX = 0;
	centerY = 0;

	for (int i = 0; i < particleCnt; ++i)
	{
		//centerX = ((float)rand() / RAND_MAX * 2.f - 1);
		//centerY = ((float)rand() / RAND_MAX * 2.f - 1);

		vertices_Position[Position_idx] = centerX - size; Position_idx++;
		vertices_Position[Position_idx] = centerY + size; Position_idx++;
		vertices_Position[Position_idx] = 0.f; Position_idx++;
		vertices_Position[Position_idx] = centerX - size; Position_idx++;
		vertices_Position[Position_idx] = centerY - size; Position_idx++;
		vertices_Position[Position_idx] = 0.f; Position_idx++;
		vertices_Position[Position_idx] = centerX + size; Position_idx++;
		vertices_Position[Position_idx] = centerY + size; Position_idx++;
		vertices_Position[Position_idx] = 0.f; Position_idx++;

		vertices_Position[Position_idx] = centerX + size; Position_idx++;
		vertices_Position[Position_idx] = centerY + size; Position_idx++;
		vertices_Position[Position_idx] = 0.f; Position_idx++;
		vertices_Position[Position_idx] = centerX - size; Position_idx++;
		vertices_Position[Position_idx] = centerY - size; Position_idx++;
		vertices_Position[Position_idx] = 0.f; Position_idx++;
		vertices_Position[Position_idx] = centerX + size; Position_idx++;
		vertices_Position[Position_idx] = centerY - size; Position_idx++;
		vertices_Position[Position_idx] = 0.f; Position_idx++;
	}

	// Color
	int Color_floatCnt = particleCnt * 6 * 4;
	float* vertices_Color = NULL;
	vertices_Color = new float[Color_floatCnt];

	int Color_idx = 0;

	float randR, randG, randB;
	randR = 0;
	randG = 0;
	randB = 0;

	for (int i = 0; i < particleCnt; ++i)
	{
		randR = ((float)rand() / RAND_MAX);
		randG = ((float)rand() / RAND_MAX);
		randB = ((float)rand() / RAND_MAX);

		vertices_Color[Color_idx] = randR; Color_idx++;
		vertices_Color[Color_idx] = randG; Color_idx++;
		vertices_Color[Color_idx] = randB; Color_idx++;
		vertices_Color[Color_idx] = 1.0f; Color_idx++;
		vertices_Color[Color_idx] = randR; Color_idx++;
		vertices_Color[Color_idx] = randG; Color_idx++;
		vertices_Color[Color_idx] = randB; Color_idx++;
		vertices_Color[Color_idx] = 1.0f; Color_idx++;
		vertices_Color[Color_idx] = randR; Color_idx++;
		vertices_Color[Color_idx] = randG; Color_idx++;
		vertices_Color[Color_idx] = randB; Color_idx++;
		vertices_Color[Color_idx] = 1.0f; Color_idx++;

		vertices_Color[Color_idx] = randR; Color_idx++;
		vertices_Color[Color_idx] = randG; Color_idx++;
		vertices_Color[Color_idx] = randB; Color_idx++;
		vertices_Color[Color_idx] = 1.0f; Color_idx++;
		vertices_Color[Color_idx] = randR; Color_idx++;
		vertices_Color[Color_idx] = randG; Color_idx++;
		vertices_Color[Color_idx] = randB; Color_idx++;
		vertices_Color[Color_idx] = 1.0f; Color_idx++;
		vertices_Color[Color_idx] = randR; Color_idx++;
		vertices_Color[Color_idx] = randG; Color_idx++;
		vertices_Color[Color_idx] = randB; Color_idx++;
		vertices_Color[Color_idx] = 1.0f; Color_idx++;
	}

	// Velocity
	int Velocity_floatCnt = particleCnt * 6 * 3;
	float* vertices_Velocity = NULL;
	vertices_Velocity = new float[Velocity_floatCnt];

	int Velocity_idx = 0;

	float randVx, randVy;
	randVx = 0;
	randVy = 0;

	for (int i = 0; i < particleCnt; ++i)
	{
		randVx = ((float)rand() / RAND_MAX * 2.f - 1);
		randVy = ((float)rand() / RAND_MAX * 2.f - 1);
	
		vertices_Velocity[Velocity_idx] = randVx; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVy; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = 0.f;  Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVx; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVy; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = 0.f;  Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVx; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVy; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = 0.f;   Velocity_idx++;

		vertices_Velocity[Velocity_idx] = randVx; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVy; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = 0.f; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVx; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVy; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = 0.f; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVx; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = randVy; Velocity_idx++;
		vertices_Velocity[Velocity_idx] = 0.f; Velocity_idx++;
	}

	// EmitTime
	int EmitTime_floatCnt = particleCnt * 6;
	float* vertices_EmitTime = NULL;
	vertices_EmitTime = new float[EmitTime_floatCnt];

	int EmitTime_idx = 0;
	float randEmitTime = 0;
	for (int i = 0; i < particleCnt; ++i)
	{
		randEmitTime = ((float)rand() / RAND_MAX) * 3;

		vertices_EmitTime[EmitTime_idx] = randEmitTime; EmitTime_idx++;
		vertices_EmitTime[EmitTime_idx] = randEmitTime; EmitTime_idx++;
		vertices_EmitTime[EmitTime_idx] = randEmitTime; EmitTime_idx++;
		vertices_EmitTime[EmitTime_idx] = randEmitTime; EmitTime_idx++;
		vertices_EmitTime[EmitTime_idx] = randEmitTime; EmitTime_idx++;
		vertices_EmitTime[EmitTime_idx] = randEmitTime; EmitTime_idx++;
	}

	// LifeTime
	int LifeTime_floatCnt = particleCnt * 6;
	float* vertices_LifeTime = NULL;
	vertices_LifeTime = new float[LifeTime_floatCnt];

	int LifeTime_idx = 0;
	float randLifeTime = 0;
	for (int i = 0; i < particleCnt; ++i)
	{
		randLifeTime = ((float)rand() / RAND_MAX) * 1;

		vertices_LifeTime[LifeTime_idx] = randLifeTime; LifeTime_idx++;
		vertices_LifeTime[LifeTime_idx] = randLifeTime; LifeTime_idx++;
		vertices_LifeTime[LifeTime_idx] = randLifeTime; LifeTime_idx++;
		vertices_LifeTime[LifeTime_idx] = randLifeTime; LifeTime_idx++;
		vertices_LifeTime[LifeTime_idx] = randLifeTime; LifeTime_idx++;
		vertices_LifeTime[LifeTime_idx] = randLifeTime; LifeTime_idx++;
	}

	// Period
	int Period_floatCnt = particleCnt * 6;
	float* vertices_Period = NULL;
	vertices_Period = new float[Period_floatCnt];

	int Period_idx = 0;
	float randPeriod = 0;
	for (int i = 0; i < particleCnt; ++i)
	{
		randPeriod = ((float)rand() / RAND_MAX) * 3;

		vertices_Period[Period_idx] = randPeriod; Period_idx++;
		vertices_Period[Period_idx] = randPeriod; Period_idx++;
		vertices_Period[Period_idx] = randPeriod; Period_idx++;
		vertices_Period[Period_idx] = randPeriod; Period_idx++;
		vertices_Period[Period_idx] = randPeriod; Period_idx++;
		vertices_Period[Period_idx] = randPeriod; Period_idx++;
	}

	// Amp
	int Amp_floatCnt = particleCnt * 6;
	float* vertices_Amp = NULL;
	vertices_Amp = new float[Amp_floatCnt];

	int Amp_idx = 0;
	float randAmp = 0;
	for (int i = 0; i < particleCnt; ++i)
	{
		randAmp = ((float)rand() / RAND_MAX * 2.f - 1.f);

		vertices_Amp[Amp_idx] = randAmp; Amp_idx++;
		vertices_Amp[Amp_idx] = randAmp; Amp_idx++;
		vertices_Amp[Amp_idx] = randAmp; Amp_idx++;
		vertices_Amp[Amp_idx] = randAmp; Amp_idx++;
		vertices_Amp[Amp_idx] = randAmp; Amp_idx++;
		vertices_Amp[Amp_idx] = randAmp; Amp_idx++;
	}

	// Theta
	int Theta_floatCnt = particleCnt * 6;
	float* vertices_Theta = NULL;
	vertices_Theta = new float[Theta_floatCnt];

	int Theta_idx = 0;
	float randTheta = 0;
	for (int i = 0; i < particleCnt; ++i)
	{
		randTheta = ((float)rand() / RAND_MAX);

		vertices_Theta[Theta_idx] = randTheta; Theta_idx++;
		vertices_Theta[Theta_idx] = randTheta; Theta_idx++;
		vertices_Theta[Theta_idx] = randTheta; Theta_idx++;
		vertices_Theta[Theta_idx] = randTheta; Theta_idx++;
		vertices_Theta[Theta_idx] = randTheta; Theta_idx++;
		vertices_Theta[Theta_idx] = randTheta; Theta_idx++;
	}

	UpdateVBOData(m_ParticleVBO_Position, Position_floatCnt, vertices_Position);
	UpdateVBOData(m_ParticleVBO_Color, Color_floatCnt, vertices_Color);
	UpdateVBOData(m_ParticleVBO_Velocity, Velocity_floatCnt, vertices_Velocity);
	UpdateVBOData(m_ParticleVBO_EmitTime, EmitTime_floatCnt, vertices_EmitTime);
	UpdateVBOData(m_ParticleVBO_LifeTime, LifeTime_floatCnt, vertices_LifeTime);
	UpdateVBOData(m_ParticleVBO_Period, Period_floatCnt, vertices_Period);
	UpdateVBOData(m_ParticleVBO_Amp, Amp_floatCnt, vertices_Amp);
	UpdateVBOData(m_ParticleVBO_Theta, Theta_floatCnt, vertices_Theta);

	delete[] vertices_Position;
	delete[] vertices_Color;
	delete[] vertices_Velocity;
	delete[] vertices_EmitTime;
	delete[] vertices_LifeTime;
	delete[] vertices_Period;
	delete[] vertices_Amp;
	delete[] vertices_Theta;

}

void Renderer::CreateParticleVertexVBO(int nParticleCnt)
{
	srand(time(NULL));

	// Particle Count
	float size = 0.01f;
	int particleCnt = nParticleCnt;
	m_ParticleVerticesCnt = particleCnt * 6;

	int floatCnt = particleCnt * 6 * (3 + 4 + 3);
	float* vertices = NULL;
	vertices = new float[floatCnt];

	int idx = 0;

	float centerX, centerY;
	centerX = 0;
	centerY = 0;

	float randR, randG, randB, randA;
	randR = 0;
	randG = 0;
	randB = 0;
	randA = 0;

	float randVx, randVy;
	randVx = 0;
	randVy = 0;

	for (int i = 0; i < particleCnt; ++i)
	{
		//centerX = ((float)rand() / RAND_MAX * 2.f - 1);
		//centerY = ((float)rand() / RAND_MAX * 2.f - 1);

		randR = ((float)rand() / RAND_MAX);
		randG = ((float)rand() / RAND_MAX);
		randB = ((float)rand() / RAND_MAX);
		randA = ((float)rand() / RAND_MAX);

		randVx = ((float)rand() / RAND_MAX * 2.f - 1);
		randVy = ((float)rand() / RAND_MAX * 2.f - 1);

		vertices[idx] = centerX - size; idx++;
		vertices[idx] = centerY + size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = randR; idx++;
		vertices[idx] = randG; idx++;
		vertices[idx] = randB; idx++;
		vertices[idx] = randA;  idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;

		vertices[idx] = centerX - size; idx++;
		vertices[idx] = centerY - size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = randR; idx++;
		vertices[idx] = randG; idx++;
		vertices[idx] = randB; idx++;
		vertices[idx] = randA;  idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;

		vertices[idx] = centerX + size; idx++;
		vertices[idx] = centerY + size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = randR; idx++;
		vertices[idx] = randG; idx++;
		vertices[idx] = randB; idx++;
		vertices[idx] = randA;  idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;

		vertices[idx] = centerX + size;idx++;
		vertices[idx] = centerY + size;idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = randR; idx++;
		vertices[idx] = randG; idx++;
		vertices[idx] = randB; idx++;
		vertices[idx] = randA;  idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;

		vertices[idx] = centerX - size;idx++;
		vertices[idx] = centerY - size;idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = randR; idx++;
		vertices[idx] = randG; idx++;
		vertices[idx] = randB; idx++;
		vertices[idx] = randA;  idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;

		vertices[idx] = centerX + size;idx++;
		vertices[idx] = centerY - size;idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = randR; idx++;
		vertices[idx] = randG; idx++;
		vertices[idx] = randB; idx++;
		vertices[idx] = randA;  idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
	}

	UpdateVBOData(m_ParticleVBO_Vertex, floatCnt, vertices);

	delete[] vertices;
}

void Renderer::UpdateVBOData(GLuint& pVBO, int nFloatCnt, float* pData)
{
	glGenBuffers(1, &pVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * nFloatCnt, pData, GL_STATIC_DRAW);
}

void Renderer::CreateFragmentSandboxVBO(int nParticleCnt)
{
	srand(time(NULL));

	float size = 1.0f;

	float centerX = 0.0f;
	float centerY = 0.0f;

	float colorR = 1.0f;
	float colorG = 1.0f;
	float colorB = 1.0f;
	float colorA = 1.0f;

	float randVx = 0;
	float randVy = 0;

	float emitTime = 0;
	float lifeTime = 0;
	float theta = 0;

	float ParticleCnt = nParticleCnt;
	// position, color, uv, vel, emitT, lifeT, theta
	int floatCnt = nParticleCnt * 6 * (3 + 4 + 2 + 3 + 1 + 1 + 1);				
	float* vertices = NULL;
	vertices = new float[floatCnt];
	m_FragmentVerticesCnt = nParticleCnt * 6;

	int idx = 0;

	for (int i = 0; i < ParticleCnt; ++i)
	{
		//centerX = ((float)rand() / RAND_MAX * 2.0f - 1);
		//centerY = ((float)rand() / RAND_MAX * 2.0f - 1);

		colorR =  ((float)rand() / RAND_MAX);
		colorG =  ((float)rand() / RAND_MAX);
		colorB =  ((float)rand() / RAND_MAX);
		colorA =  ((float)rand() / RAND_MAX);

		randVx =  ((float)rand() / RAND_MAX * 2.0f - 1);
		randVy =  ((float)rand() / RAND_MAX * 2.0f - 1);

		emitTime = ((float)rand() / RAND_MAX) * 3.0f;
		lifeTime = ((float)rand() / RAND_MAX) * 1.0;
		theta = ((float)rand() / RAND_MAX);

		vertices[idx] = centerX - size; idx++;
		vertices[idx] = centerY + size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = colorR; idx++;
		vertices[idx] = colorG; idx++;
		vertices[idx] = colorB; idx++;
		vertices[idx] = colorA; idx++;
		vertices[idx] = 0.0f; idx++;
		vertices[idx] = 0.0f; idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
		vertices[idx] = emitTime;  idx++;
		vertices[idx] = lifeTime;  idx++;
		vertices[idx] = theta;  idx++;

		vertices[idx] = centerX - size; idx++;
		vertices[idx] = centerY - size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = colorR; idx++;
		vertices[idx] = colorG; idx++;
		vertices[idx] = colorB; idx++;
		vertices[idx] = colorA;  idx++;
		vertices[idx] = 0.0f; idx++;
		vertices[idx] = 1.0f; idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
		vertices[idx] = emitTime;  idx++;
		vertices[idx] = lifeTime;  idx++;
		vertices[idx] = theta;  idx++;

		vertices[idx] = centerX + size; idx++;
		vertices[idx] = centerY + size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = colorR; idx++;
		vertices[idx] = colorG; idx++;
		vertices[idx] = colorB; idx++;
		vertices[idx] = colorA;  idx++;
		vertices[idx] = 1.0f; idx++;
		vertices[idx] = 0.0f; idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
		vertices[idx] = emitTime;  idx++;
		vertices[idx] = lifeTime;  idx++;
		vertices[idx] = theta;  idx++;

		vertices[idx] = centerX + size; idx++;
		vertices[idx] = centerY + size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = colorR; idx++;
		vertices[idx] = colorG; idx++;
		vertices[idx] = colorB; idx++;
		vertices[idx] = colorA;  idx++;
		vertices[idx] = 1.0f; idx++;
		vertices[idx] = 0.0f; idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
		vertices[idx] = emitTime;  idx++;
		vertices[idx] = lifeTime;  idx++;
		vertices[idx] = theta;  idx++;

		vertices[idx] = centerX - size; idx++;
		vertices[idx] = centerY - size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = colorR; idx++;
		vertices[idx] = colorG; idx++;
		vertices[idx] = colorB; idx++;
		vertices[idx] = colorA;  idx++;
		vertices[idx] = 0.0f; idx++;
		vertices[idx] = 1.0f; idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
		vertices[idx] = emitTime;  idx++;
		vertices[idx] = lifeTime;  idx++;
		vertices[idx] = theta;  idx++;

		vertices[idx] = centerX + size; idx++;
		vertices[idx] = centerY - size; idx++;
		vertices[idx] = 0.f; idx++;
		vertices[idx] = colorR; idx++;
		vertices[idx] = colorG; idx++;
		vertices[idx] = colorB; idx++;
		vertices[idx] = colorA; idx++;
		vertices[idx] = 1.0f; idx++;
		vertices[idx] = 1.0f; idx++;
		vertices[idx] = randVx; idx++;
		vertices[idx] = randVy; idx++;
		vertices[idx] = 0.f;  idx++;
		vertices[idx] = emitTime;  idx++;
		vertices[idx] = lifeTime;  idx++;
		vertices[idx] = theta;  idx++;
	}

	
	UpdateVBOData(m_FragmentSandboxVBO, floatCnt, vertices);

	delete[] vertices;
}

void Renderer::CreateFragmentSandbox_BlendVBO()
{
	float centerX = 0;
	float centerY = 0;

	float size = 1;


	float ParticleCnt = 1;
	int floatCnt = 1 * 6 * (3);
	float* vertices = NULL;
	vertices = new float[floatCnt];

	int idx = 0;

	vertices[idx] = centerX - size; idx++;
	vertices[idx] = centerY + size; idx++;
	vertices[idx] = 0.f; idx++;

	vertices[idx] = centerX - size; idx++;
	vertices[idx] = centerY - size; idx++;
	vertices[idx] = 0.f; idx++;

	vertices[idx] = centerX + size; idx++;
	vertices[idx] = centerY + size; idx++;
	vertices[idx] = 0.f; idx++;

	vertices[idx] = centerX + size; idx++;
	vertices[idx] = centerY + size; idx++;
	vertices[idx] = 0.f; idx++;

	vertices[idx] = centerX - size; idx++;
	vertices[idx] = centerY - size; idx++;
	vertices[idx] = 0.f; idx++;

	vertices[idx] = centerX + size; idx++;
	vertices[idx] = centerY - size; idx++;
	vertices[idx] = 0.f; idx++;


	UpdateVBOData(m_FragmentSandbox_BlendVBO, floatCnt, vertices);

	delete[] vertices;
}

void Renderer::CreateVertexMovementVBO(int nParticleCnt)
{
	float startX = -1;
	float startY = 0;

	float stride = 2.f / ((float)nParticleCnt - 1.f);

	float ParticleCnt = nParticleCnt;
	// Pos
	int floatCnt = ParticleCnt * 6 * (3);
	float* vertices = NULL;
	vertices = new float[floatCnt];
	m_VertexMovementVerticesCnt = ParticleCnt;

	int idx = 0;

	for (int i = 0; i < ParticleCnt; ++i)
	{
		vertices[idx] = startX + (stride * i); idx++;
		vertices[idx] = startY; idx++;
		vertices[idx] = 0.f; idx++;
	}

	UpdateVBOData(m_VertexMovementVBO, floatCnt, vertices);

	delete[] vertices;
}

void Renderer::Class0310_Render()
{
	//Program select
	int shaderProgram = m_SolidRectShader;
	glUseProgram(shaderProgram);

	glUniform4f(glGetUniformLocation(shaderProgram, "u_Trans"), 0, 0, 0, 1);
	glUniform4f(glGetUniformLocation(shaderProgram, "u_Color"), 1, 1, 1, 1);


	// Pos0
	int attribLoc_Position = -1;
	attribLoc_Position = glGetAttribLocation(shaderProgram, "a_Position");
	glEnableVertexAttribArray(attribLoc_Position);
	glBindBuffer(GL_ARRAY_BUFFER, m_testVBO_Pos0);
	glVertexAttribPointer(attribLoc_Position, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Pos1
	int attribLoc_Position1 = -1;
	attribLoc_Position1 = glGetAttribLocation(shaderProgram, "a_Position1");
	glEnableVertexAttribArray(attribLoc_Position1);
	glBindBuffer(GL_ARRAY_BUFFER, m_testVBO_Pos1);
	glVertexAttribPointer(attribLoc_Position1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Color
	int attribLoc_Color = -1;
	attribLoc_Color = glGetAttribLocation(shaderProgram, "a_Color");
	glEnableVertexAttribArray(attribLoc_Color);
	glBindBuffer(GL_ARRAY_BUFFER, m_testVBO_Color);
	glVertexAttribPointer(attribLoc_Color, 4, GL_FLOAT, GL_FALSE, 0, 0);

	int uniformLoc_Scale = -1;
	uniformLoc_Scale = glGetUniformLocation(shaderProgram, "u_Scale");
	glUniform1f(uniformLoc_Scale, m_TotalTime);

	glDrawArrays(GL_TRIANGLES, 0, 3);
}

void Renderer::DrawParticleEffect()
{
	//Program select
	int shaderProgram = m_ParticleShader;
	glUseProgram(shaderProgram);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//// Pos0
	//int attribLoc_Position = -1;
	//attribLoc_Position = glGetAttribLocation(shaderProgram, "a_Position");
	//glEnableVertexAttribArray(attribLoc_Position);
	//glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Position);
	//glVertexAttribPointer(attribLoc_Position, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//// Color
	//int attribLoc_Color = -1;
	//attribLoc_Color = glGetAttribLocation(shaderProgram, "a_Color");
	//glEnableVertexAttribArray(attribLoc_Color);
	//glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Color);
	//glVertexAttribPointer(attribLoc_Color, 4, GL_FLOAT, GL_FALSE, 0, 0);

	//// Velocity
	//int attribLoc_Velocity = -1;
	//attribLoc_Velocity = glGetAttribLocation(shaderProgram, "a_Velocity");
	//glEnableVertexAttribArray(attribLoc_Velocity);
	//glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Velocity);
	//glVertexAttribPointer(attribLoc_Velocity, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// PosColorVelVertex
	int attribLoc_Position = -1;
	attribLoc_Position = glGetAttribLocation(shaderProgram, "a_Position");

	int attribLoc_Color = -1;
	attribLoc_Color = glGetAttribLocation(shaderProgram, "a_Color");

	int attribLoc_Velocity = -1;
	attribLoc_Velocity = glGetAttribLocation(shaderProgram, "a_Velocity");

	glEnableVertexAttribArray(attribLoc_Position);
	glEnableVertexAttribArray(attribLoc_Color);
	glEnableVertexAttribArray(attribLoc_Velocity);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Vertex);
	glVertexAttribPointer(attribLoc_Position, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 10, 0);
	glVertexAttribPointer(attribLoc_Color, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 10, (void*)(sizeof(float) * 3));
	glVertexAttribPointer(attribLoc_Velocity, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 10, (void*)(sizeof(float) * 7));
	

	// EmitTime
	int attribLoc_EmitTime = -1;
	attribLoc_EmitTime = glGetAttribLocation(shaderProgram, "a_EmitTime");
	glEnableVertexAttribArray(attribLoc_EmitTime);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_EmitTime);
	glVertexAttribPointer(attribLoc_EmitTime, 1, GL_FLOAT, GL_FALSE, 0, 0);

	// LifeTime
	int attribLoc_LifeTime = -1;
	attribLoc_LifeTime = glGetAttribLocation(shaderProgram, "a_LifeTime");
	glEnableVertexAttribArray(attribLoc_LifeTime);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_LifeTime);
	glVertexAttribPointer(attribLoc_LifeTime, 1, GL_FLOAT, GL_FALSE, 0, 0);

	// Period
	int attribLoc_Period = -1;
	attribLoc_Period = glGetAttribLocation(shaderProgram, "a_Period");
	glEnableVertexAttribArray(attribLoc_Period);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Period);
	glVertexAttribPointer(attribLoc_Period, 1, GL_FLOAT, GL_FALSE, 0, 0);

	// Amp
	int attribLoc_Amp = -1;
	attribLoc_Amp = glGetAttribLocation(shaderProgram, "a_Amp");
	glEnableVertexAttribArray(attribLoc_Amp);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Amp);
	glVertexAttribPointer(attribLoc_Amp, 1, GL_FLOAT, GL_FALSE, 0, 0);

	// Theta
	int attribLoc_Theta = -1;
	attribLoc_Theta = glGetAttribLocation(shaderProgram, "a_Theta");
	glEnableVertexAttribArray(attribLoc_Theta);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleVBO_Theta);
	glVertexAttribPointer(attribLoc_Theta, 1, GL_FLOAT, GL_FALSE, 0, 0);


	// Uniform TotalTime
	int uniformLoc_TotalTime = -1;
	uniformLoc_TotalTime = glGetUniformLocation(shaderProgram, "u_TotalTime");
	glUniform1f(uniformLoc_TotalTime, m_TotalTime);


	m_TotalTime += 0.0001f;

	glDrawArrays(GL_TRIANGLES, 0, m_ParticleVerticesCnt);

	glDisable(GL_BLEND);
}

void Renderer::DrawFragmentSandbox()
{
	int shaderProgram = m_FragmentSandboxShader;
	glUseProgram(shaderProgram);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// PosColorVelVertex
	int attribLoc_Position = -1;
	attribLoc_Position = glGetAttribLocation(shaderProgram, "a_Position");
	int attribLoc_Color = -1;
	attribLoc_Color = glGetAttribLocation(shaderProgram, "a_Color");
	int attribLoc_Texcoord = -1;
	attribLoc_Texcoord = glGetAttribLocation(shaderProgram, "a_Texcoord");
	int attribLoc_Velocity = -1;
	attribLoc_Velocity = glGetAttribLocation(shaderProgram, "a_Velocity");
	int attribLoc_EmitTime = -1;
	attribLoc_EmitTime = glGetAttribLocation(shaderProgram, "a_EmitTime");
	int attribLoc_LifeTime = -1;
	attribLoc_LifeTime = glGetAttribLocation(shaderProgram, "a_LifeTime");
	int attribLoc_Theta = -1;
	attribLoc_Theta = glGetAttribLocation(shaderProgram, "a_Theta");

	// position, color, uv, vel, emitT, lifeT, theta

	glEnableVertexAttribArray(attribLoc_Position);
	glEnableVertexAttribArray(attribLoc_Color);
	glEnableVertexAttribArray(attribLoc_Texcoord);
	glEnableVertexAttribArray(attribLoc_Velocity);
	glEnableVertexAttribArray(attribLoc_EmitTime);
	glEnableVertexAttribArray(attribLoc_LifeTime);
	glEnableVertexAttribArray(attribLoc_Theta);
	glBindBuffer(GL_ARRAY_BUFFER, m_FragmentSandboxVBO);
	glVertexAttribPointer(attribLoc_Position, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, 0);
	glVertexAttribPointer(attribLoc_Color,	  4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (void*)(sizeof(float) * 3));
	glVertexAttribPointer(attribLoc_Texcoord, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (void*)(sizeof(float) * 7));
	glVertexAttribPointer(attribLoc_Velocity, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (void*)(sizeof(float) * 9));
	glVertexAttribPointer(attribLoc_EmitTime, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (void*)(sizeof(float) * 12));
	glVertexAttribPointer(attribLoc_LifeTime, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (void*)(sizeof(float) * 13));
	glVertexAttribPointer(attribLoc_Theta, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (void*)(sizeof(float) * 14));

	int uniformLoc_Time = -1;
	uniformLoc_Time = glGetUniformLocation(shaderProgram, "u_Time");
	glUniform1f(uniformLoc_Time, m_TotalTime);

	int uniformLoc_Center = -1;
	uniformLoc_Center = glGetUniformLocation(shaderProgram, "u_Center");
	glUniform3f(uniformLoc_Center, m_uniformCenterX, m_uniformCenterY, 0);

	int uniformLoc_Velocity = -1;
	uniformLoc_Velocity = glGetUniformLocation(shaderProgram, "u_Velocity");
	glUniform3f(uniformLoc_Velocity, m_uniformVelocityX, m_uniformVelocityY, 0);

	int uniformLoc_Point = -1;
	uniformLoc_Point = glGetUniformLocation(shaderProgram, "u_Point");
	glUniform3f(uniformLoc_Point, m_uniformPointX, m_uniformPointY, 0);

	float points[] = { 0.5f, 0.5f, 0.0f,
					   0.0f, 0.0f, 0.0f,
					   1.0f, 1.0f, 0.0f };

	int uniformLoc_Points = -1;
	uniformLoc_Points = glGetUniformLocation(shaderProgram, "u_Points");
	glUniform3fv(uniformLoc_Points, 3, points);

	m_TotalTime += 0.00003f;

	glDrawArrays(GL_TRIANGLES, 0, m_FragmentVerticesCnt);

	glDisable(GL_BLEND);
}

void Renderer::DrawFragmentSandboxBlend()
{
	int shaderProgram = m_FragmentSandbox_BlendShader;
	glUseProgram(shaderProgram);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// PosColorVelVertex
	int attribLoc_Position = -1;
	attribLoc_Position = glGetAttribLocation(shaderProgram, "a_Position");

	glEnableVertexAttribArray(attribLoc_Position);
	glBindBuffer(GL_ARRAY_BUFFER, m_FragmentSandbox_BlendVBO);
	glVertexAttribPointer(attribLoc_Position, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);


	glDisable(GL_BLEND);
}

void Renderer::DrawVertexMovement()
{
	int shaderProgram = m_VertexMovementShader;
	glUseProgram(shaderProgram);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	// PosColorVelVertex
	int attribLoc_Position = -1;
	attribLoc_Position = glGetAttribLocation(shaderProgram, "a_Position");

	glEnableVertexAttribArray(attribLoc_Position);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexMovementVBO);
	glVertexAttribPointer(attribLoc_Position, 3, GL_FLOAT, GL_FALSE, 0, 0);

	int uniformLoc_Time = -1;
	uniformLoc_Time = glGetUniformLocation(shaderProgram, "u_Time");
	glUniform1f(uniformLoc_Time, m_TotalTime);

	m_TotalTime += 0.0008f;
	float time = m_TotalTime;

	glDrawArrays(GL_LINE_STRIP, 0, m_VertexMovementVerticesCnt);

	time += 1.0f;

	glUniform1f(uniformLoc_Time, time);
	glDrawArrays(GL_LINE_STRIP, 0, m_VertexMovementVerticesCnt);



	glDisable(GL_BLEND);
}

void Renderer::Class0310()
{
	// Pos0
	float vertices[] = { 
		0.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f, 
		1.0f, 1.0f, 0.0f };	// CPU Memory
	glGenBuffers(1, &m_testVBO_Pos0);	// get Buffer Object ID
	glBindBuffer(GL_ARRAY_BUFFER, m_testVBO_Pos0); // Bind to array buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	
	// Pos1
	float vertices1[] = {
		-1.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f, 
		0.0f, 0.0f, 0.0f };	// CPU Memory
	glGenBuffers(1, &m_testVBO_Pos1);	// get Buffer Object ID
	glBindBuffer(GL_ARRAY_BUFFER, m_testVBO_Pos1); // Bind to array buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);

	// Color
	float vColors[] = { 
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f };	// CPU Memory
	glGenBuffers(1, &m_testVBO_Color);	// get Buffer Object ID
	glBindBuffer(GL_ARRAY_BUFFER, m_testVBO_Color); // Bind to array buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(vColors), vColors, GL_STATIC_DRAW);

}
