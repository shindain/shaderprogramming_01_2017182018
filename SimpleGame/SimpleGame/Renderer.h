#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	bool IsInitialized();
	void DrawSolidRect(float x, float y, float z, float size, float r, float g, float b, float a);

	// =================================

	void Class0310();
	void Class0310_Render();
	void DrawParticleEffect();

	// =================================
	void DrawFragmentSandbox();
	void DrawFragmentSandboxBlend();

	// =================================
	void DrawVertexMovement();

private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects();
	void GetGLPosition(float x, float y, float *newX, float *newY);

	bool m_Initialized = false;
	
	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	GLuint m_VBORect = 0;
	GLuint m_SolidRectShader = 0;

	// ================================

	GLuint m_testVBO_Pos0 = 0;
	GLuint m_testVBO_Pos1 = 0;
	GLuint m_testVBO_Color = 0;

	// Particle
	GLuint m_ParticleShader = -1;

	GLuint m_ParticleVBO_Position = -1;
	GLuint m_ParticleVBO_Color = -1;
	GLuint m_ParticleVBO_Velocity = -1;
	GLuint m_ParticleVBO_EmitTime = -1;
	GLuint m_ParticleVBO_LifeTime = -1;
	GLuint m_ParticleVBO_Period = -1;
	GLuint m_ParticleVBO_Amp = -1;
	GLuint m_ParticleVBO_Theta = -1;

	GLuint m_ParticleVBO_Vertex = -1;

	GLuint m_ParticleVerticesCnt = -1;

	float m_fSpeed = 0.1f;
	float m_TotalTime = 0.f;
	float m_Gravity = 0.009f;

	void CreateParticleVBO(int nParticleCnt);
	void CreateParticleVertexVBO(int nParticleCnt);
	void UpdateVBOData(GLuint& pVBO, int nFloatCnt, float* pData);

	// Fragment Test
	GLuint m_FragmentSandboxShader = -1;
	GLuint m_FragmentSandboxVBO = -1;

	GLuint m_FragmentSandbox_BlendShader = -1;
	GLuint m_FragmentSandbox_BlendVBO = -1;

	float m_uniformCenterX = 0.0f;
	float m_uniformCenterY = 0.0f;

	float m_uniformPointX = 0.5f;
	float m_uniformPointY = 0.5f;

	float m_uniformVelocityX = -1.0f;
	float m_uniformVelocityY = -1.0f;

	GLuint m_FragmentVerticesCnt = -1;

	void CreateFragmentSandboxVBO(int nParticleCnt);
	void CreateFragmentSandbox_BlendVBO();

	// Movement Vertex
	GLuint m_VertexMovementShader = -1;
	GLuint m_VertexMovementVBO = -1;



	GLuint m_VertexMovementVerticesCnt = -1;


	void CreateVertexMovementVBO(int nParticleCnt);
};

