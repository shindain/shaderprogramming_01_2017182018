#version 330

in vec4 v_Color;
in vec2 v_Texcoord;
layout(location=0) out vec4 FragColor;

uniform vec3 u_Point;
uniform vec3 u_Points[3];
uniform float u_Time;

const float c_PI = 3.141592f;


void test()
{
	float newValueX = v_Texcoord.x * 10.0f * c_PI;
	float newValueY = v_Texcoord.y * 10.0f * c_PI;
	float outColorVertical = sin(newValueX);
	float outColorHorizontal = sin(newValueY);

	vec4 newColor = vec4(max(outColorHorizontal, outColorVertical));

	FragColor = vec4(newColor);

}

void circle()
{
	vec2 tmpVec = v_Texcoord - u_Point.xy;
	float d = length(tmpVec);

	if(d < 0.1f)
	{
		FragColor = vec4(v_Color);
	}
	else
	{
		FragColor = vec4(0.0f);
	}
}

void circles()
{
	vec2 tmpVec = v_Texcoord;
	tmpVec.x -= 0.5f;
	tmpVec.y -= 0.5f;

	float d = length(tmpVec);			// 0 ~ sqrt(0.5 * 0.5)
	float value = sin(d * 2 * 3.14 * 2);


	FragColor = vec4(value);

}

void justColor()
{
	FragColor = v_Color;
}

void radar()
{
	vec2 tmpVec = v_Texcoord - vec2(0.5, 0.5);
	float d = length(tmpVec);
	float value = 0.2f * (pow(sin(d * c_PI * 2 - 50 * u_Time), 12) - 0.5);
	float temp = ceil(value);

	vec4 result = vec4(0);
	for(int i = 0 ; i < 3; ++i)
	{
		vec2 tmpVec = v_Texcoord - u_Points[i].xy;
		float d = length(tmpVec);
		
		if(d < 0.1)
		{
			result += 1.0f * temp;
		}
	}
	


	FragColor = vec4(result + 10 * value);

}

void flag()
{
	float newColor = sin(v_Texcoord.x * c_PI * 2) / 2;
	float width = 0.01f;

	if((v_Texcoord.y - 0.5) * 2 < newColor + width && 
		(v_Texcoord.y - 0.5) * 2 > newColor - width)
	{
		FragColor = vec4(1);
	}
	else
	{
		FragColor = vec4(0);
	}
}

void Moveflag()
{
	FragColor = vec4(0);

	float sinAlpha = sin(v_Texcoord.x * c_PI * 100);

	float range = v_Texcoord.x / 2;

	float newColor = sin(v_Texcoord.x * c_PI * 2 - u_Time  * 100) * range;
	float width = 0.005f;

	if((v_Texcoord.y - 0.5) * 2 < newColor + width && 
		(v_Texcoord.y - 0.5) * 2 > newColor - width)
	{
		FragColor = vec4(1, 1, 1, sinAlpha);
	}
}

void MultiMoveflag()
{
	FragColor = vec4(0);

	float sinAlpha = sin(v_Texcoord.x * c_PI * 100);

	float range = v_Texcoord.x / 2;

	float Time1 = u_Time;
	float Time2 = u_Time / 3;

	float newColor1 = sin(v_Texcoord.x * c_PI * 2 - Time1  * 100) * range;
	float newColor2 = sin(v_Texcoord.x * c_PI * 2 - Time2  * 100) * range * 2;
	float width = 0.005f;

	for(int i = -2 ; i < 3; ++i)
	{
		if((v_Texcoord.y - 0.5) * 2 < newColor1 + width + i * 0.3f && 
		(v_Texcoord.y - 0.5) * 2 > newColor1 - width + i * 0.3f)
		{
			FragColor = vec4(1, 1, 1, sinAlpha);
		}
	}

	if((v_Texcoord.y - 0.5) * 2 < newColor1 + width && 
		(v_Texcoord.y - 0.5) * 2 > newColor1 - width)
	{
		FragColor = vec4(1, 1, 1, sinAlpha);
	}
	else if((v_Texcoord.y - 0.5) * 2 < newColor2 + width && 
			(v_Texcoord.y - 0.5) * 2 > newColor2 - width)
	{
		FragColor = vec4(1, 1, 1, sinAlpha);
	}
}

void LectureMultiFlag()
{
	float finalColor = 0;

	for(int i = 0 ; i < 1; ++i)
	{
		float newTime = u_Time + i * 0.5;
		float sinAlpha = sin(v_Texcoord.x * c_PI * 20);

		float range = v_Texcoord.x / 2;

		float newColor = sin(v_Texcoord.x * c_PI * 2 - newTime  * 100) * range;
		float width = 0.05f * v_Texcoord.x;

		if((v_Texcoord.y - 0.5) * 2 < newColor + width && 
			(v_Texcoord.y - 0.5) * 2 > newColor - width)
		{
			finalColor += 1 * sinAlpha;
		}
	}

	
	FragColor = vec4(finalColor);
}

void main()
{
	//test();
	//circle();
	//circles();
	//justColor();
	//radar();

	//flag();
	//Moveflag();
	//MultiMoveflag();
	LectureMultiFlag();
}
