#version 330

in float v_Alpha;

layout(location=0) out vec4 FragColor;

void main()
{
	float newLine = sin(100 * (1 - v_Alpha));

	FragColor = vec4(1, 1, 1, newLine);
}
