#version 330

in vec3 a_Position;

out float v_Alpha;

uniform float u_Time;
const float c_PI = 3.141592;

void just()
{
	gl_Position = vec4(a_Position, 1.0f);
}

void drawSinCurve()
{
	vec4 newPosition = vec4(a_Position, 1.0f);

	float value = (newPosition.x + 1) * c_PI;
	float newY = sin(value - u_Time);

	newPosition.y = newY;

	gl_Position = newPosition;
}

void drawFlagCurve()
{
	vec4 newPosition = vec4(a_Position, 1.0f);

	float rangebyx = (newPosition.x + 1) / 2;
	float value = rangebyx * c_PI;
	float newY = sin(value - u_Time) * rangebyx;

	newPosition.y = newY;
	gl_Position = newPosition;
}

void drawFlagCurveAlpha()
{
	vec4 newPosition = vec4(a_Position, 1.0f);

	float rangebyx = (newPosition.x + 1) / 2;
	float value = rangebyx * c_PI;
	float newY = sin(value - u_Time) * rangebyx / 2;

	newPosition.y = newY;
	gl_Position = newPosition;

	float alpha = (newPosition.x + 1) / 2;

	v_Alpha = 1 - alpha;
}

void main()
{
	drawFlagCurveAlpha();
}
