#version 330

in vec3 a_Position;
in vec4 a_Color;		
in vec2 a_Texcoord;		
in vec3 a_Velocity;
in float a_EmitTime;
in float a_LifeTime;
in float a_Theta;


out vec4 v_Color;
out vec2 v_Texcoord;

const float c_Pi = 3.14;
const float c_Period = 1.0f;
const float c_Amp = 0.3f;
const float c_Radius = 1.0f;

const vec3 c_Velocity = vec3(0.0f, 0.7f, 0.0f);


uniform float u_Time;
uniform vec3 u_Center;
uniform vec3 u_Velocity;

void just()
{
	gl_Position = vec4(a_Position, 1.0f);

	v_Color = a_Color;
	v_Texcoord = a_Texcoord;
}

void simpleSinParticle()
{
	vec4 newPosition = vec4(0, 0, 0, 1);
	vec4 newColor = a_Color;
	float calcTime = u_Time - a_EmitTime;
	float rangeTime = fract(u_Time) * 2.0f;

	if(calcTime < 0)
	{
		
	}
	else
	{
		float newTime = fract(calcTime / a_LifeTime) * a_LifeTime;

		float nX = c_Radius * cos(a_Theta * 2 * c_Pi);
		float nY = c_Radius * sin(a_Theta * 2 * c_Pi);

		newPosition.x = a_Position.x + a_Velocity.x * newTime + nX;
		newPosition.y = a_Position.y + a_Velocity.y * newTime + nY;
		
		vec2 newDir = vec2(-a_Velocity.y, a_Velocity.x);
		newDir = normalize(newDir);
		newPosition.xy += newDir * newTime * c_Amp * sin(newTime * 2 * c_Pi * c_Period);



		newColor.a = newColor.a * (1 - fract(calcTime / a_LifeTime));

	}
	
	gl_Position = newPosition;
	v_Color = newColor;
	v_Texcoord = a_Texcoord;
}

void meteorParticle()
{
	vec4 newPosition = vec4(0, 0, 0, 1);
	vec4 newColor = a_Color;
	float calcTime = u_Time - a_EmitTime;
	float rangeTime = fract(u_Time) * 2.0f;

	if(calcTime < 0)
	{
		
	}
	else
	{
		float newTime = fract(calcTime / a_LifeTime) * a_LifeTime;

		float nX = c_Radius * cos(a_Theta * 2 * c_Pi) + u_Center.x + u_Velocity.x * rangeTime;
		float nY = c_Radius * sin(a_Theta * 2 * c_Pi) + u_Center.y + u_Velocity.y * rangeTime;

		newPosition.x = a_Position.x + u_Velocity.x * newTime * -1 + nX;
		newPosition.y = a_Position.y + u_Velocity.y * newTime * -1 + nY;
		
		vec2 newDir = vec2(-u_Velocity.y, u_Velocity.x);
		newDir = normalize(newDir);
		newPosition.xy += newDir * newTime * c_Amp * sin(newTime * 2 * c_Pi * c_Period);



		newColor.a = newColor.a * (1 - fract(calcTime / a_LifeTime));

	}
	
	gl_Position = newPosition;
	v_Color = newColor;
	v_Texcoord = a_Texcoord;

}

void thePositionParticle()
{
	vec4 newPosition = vec4(0, 0, 0, 1);
	vec4 newColor = a_Color;
	float calcTime = u_Time - a_EmitTime;

	if(calcTime < 0)
	{
		
	}
	else
	{
		float newTime = fract(calcTime / a_LifeTime) * a_LifeTime;

		float nX = c_Radius * cos(a_Theta * 2 * c_Pi) + u_Center.x;
		float nY = c_Radius * sin(a_Theta * 2 * c_Pi) + u_Center.y;

		newPosition.x = a_Position.x + a_Velocity.x * newTime + nX;
		newPosition.y = a_Position.y + a_Velocity.y * newTime + nY;
		
		vec2 newDir = vec2(-a_Velocity.y, a_Velocity.x);
		newDir = normalize(newDir);
		newPosition.xy += newDir * newTime * c_Amp * sin(newTime * 2 * c_Pi * c_Period);

		newColor.a = newColor.a * (1 - fract(calcTime / a_LifeTime));

	}
	
	gl_Position = newPosition;

	v_Color = a_Color;
	v_Texcoord = a_Texcoord;
}

void main()
{
	just();
	//simpleSinParticle();
	//meteorParticle();

	//thePositionParticle();
}
