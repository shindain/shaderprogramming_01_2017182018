#version 330

in vec3 a_Position;		// ATTRIBUTE (VS INPUT)
in vec4 a_Color;		// ATTRIBUTE (VS INPUT)
in vec3 a_Velocity;		// ATTRIBUTE (VS INPUT)
in float a_EmitTime;
in float a_LifeTime;
in float a_Period;
in float a_Amp;
in float a_Theta;

out vec4 v_Color;

uniform float u_TotalTime;

const vec3 c_Gravity = vec3(0.0f, -1.8f, 0.0f);
const float c_LifeTime = 2.0f;
const vec3 c_Vel = vec3(0.0f, -0.7f, 0.0f);
const float c_Pi = 3.14;
const float c_Period = 1.0f;
const float c_Amp = 1.0f;
const float c_Radius = 1.0f;

void GraphSin()
{
	float Calctime = u_TotalTime - a_EmitTime;
	vec4 newPosition = vec4(0,0,0,1);
	vec4 newColor = a_Color;

	if(Calctime < 0)
	{

	}
	else
	{
		float newTime = fract(Calctime / a_LifeTime) * a_LifeTime;
		float nX = c_Radius * cos(a_Theta * 2 * c_Pi);
		float nY = c_Radius * sin(a_Theta * 2 * c_Pi);

		newPosition.x = a_Position.x + a_Velocity.x * newTime + nX;
		newPosition.y = a_Position.y + a_Velocity.y * newTime + nY;
		
		vec2 newDir = vec2(-a_Velocity.y, a_Velocity.x);
		newDir = normalize(newDir);
		newPosition.xy += newDir * newTime * a_Amp * sin(newTime * 2 * c_Pi * a_Period);

		newColor.a = newColor.a * ( 1 - fract(Calctime / a_LifeTime)) ; 
		newColor.a = pow(newColor.a, 0.5);
	}

	gl_Position = newPosition;

	v_Color = newColor;
}

void P1()
{
	float Calctime = u_TotalTime - a_EmitTime;
	vec4 newPosition = vec4(0,0,0,1);
	vec4 newColor = a_Color;
	

	if(Calctime < 0)
	{

	}
	else
	{
		float newTime = fract(Calctime / a_LifeTime) * a_LifeTime;

		newPosition.xyz = a_Position;
		newPosition.xyz += (newTime * a_Velocity);
		newPosition.xyz += 0.5 * c_Gravity * newTime * newTime;

		newColor.a = newColor.a * ( 1 - fract(Calctime / a_LifeTime)) ; 
	}

	newPosition.w = 1;

	gl_Position = newPosition;

	v_Color = newColor;
}

void main()
{
	GraphSin();
}
